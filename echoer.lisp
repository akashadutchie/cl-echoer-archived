(in-package :cl-echoer)

(defstruct (echoer (:constructor mkech) (:conc-name nil))
  file-differentiator-string 
  dir
  (auto-track t)
  tracked
  (host# 0)
  (host->num (list))
  (hostnum->process (list)))

(defun track-filepath (echoer track#)
  (merge-pathnames (format nil ".~A~A.echoer-tracked.tmp" track# (file-differentiator-string echoer)) (dir echoer)))

(defun %track-copy-filepath (echoer track#)
  (merge-pathnames (format nil ".~A~A.echoer-tracked.tmp1" track# (file-differentiator-string echoer)) (dir echoer)))

(defun writeback-filepath (echoer)
  (merge-pathnames (format nil ".~A.echoer-writeback.tmp" (file-differentiator-string echoer)) (dir echoer)))

(defun make-echoer (&optional (file-differentiator-string "")
                      (echoer-directory (merge-pathnames #P"/tmp/echoer/" (package-name *package*)))
                      (watch-cap 8))  
  (ensure-directories-exist echoer-directory) 
  (let ((echoer (mkech
                 :dir echoer-directory 
                 :file-differentiator-string file-differentiator-string
                 :tracked (make-array `(,watch-cap) :initial-element nil))))
    (loop for i from 0 below watch-cap 
          do (let ((trackfp (track-filepath echoer i)));(merge-pathnames (format nil ".~A" i) homedir)
               ;; Create blank files for tracking to initially latch on to
               ;; Don't delete file, it causes persistent watchers to detach from the original pipes
               ;; (if (probe-file trackfp)
               ;;     (delete-file trackfp))
               (unless (probe-file trackfp) 
                 (uiop:run-program `("mkfifo" ,(namestring trackfp)))))) 
    ;; Create writeback file
    ;;TODO make sure it is a named pipe that this program generated 
    (unless (uiop:file-exists-p (writeback-filepath echoer))
      (uiop:run-program `("mkfifo" ,(namestring (writeback-filepath echoer)))))
    echoer))

(defun highlight (echoer host slot#)
  "Track HOST into SLOT# for the consumer program of echoer"
  (%try-initialise echoer host nil nil)
  (%highlight echoer host slot#))

(defun %try-initialise (echoer host)
  (let ((host-num (getf (host->num echoer) host))
        new-hostnum
        autotrack
        isnt-tracked)
    (unless host-num
      ;; Create host number
      (push (incf (host# echoer)) (host->num echoer)) 
      (push host (host->num echoer))
      (setq host-num (host# echoer)
            new-hostnum t))
    (setq isnt-tracked (not (find host-num (tracked echoer))))
    (setq autotrack
          (let ((at (auto-track echoer))
                (position (position nil (tracked echoer))))
            (if (and at position isnt-tracked)
                (%highlight echoer host position))))))

(defun %highlight (echoer host slot#)
  (let ((hostnum (getf (host->num echoer) host))
        (old-hostnum (aref (tracked echoer) slot#)))

    ;; Mute other highlights of the same host
    ;; Otherwise they will step on each other
    (loop for a across (tracked echoer)
          for i from 0
          if (eq hostnum a)
            do (setf (aref a i) nil))
    
    (setf (aref (tracked echoer) slot#) hostnum)))

(defun message (echoer host message)
  (%try-initialise echoer host)
  (when-let ((pos (position (getf (host->num echoer) host) (tracked echoer))))
    (write-string-into-file message (track-filepath echoer pos)
                            :if-exists :append)
    pos))

(defun update (echoer)
  "Consume writebacks from tracked scripts. Returns a values list containing:
1. the message
2. the host"
  (when-let* (
              ;; Ensure *something* is written to the writeback, or else
              ;; it will freeze waiting for something to be written when opened
              (dummy (uiop:launch-program `("cat" "\"\"" ">" ,(writeback-filepath echoer))))
              (file (open (writeback-filepath echoer) :if-does-not-exist nil))
              (host (read-line file nil))
              (cmd (read-line file nil)))
    (values cmd host)))
