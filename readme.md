﻿# What

Specialised tool for watching multiple streams of messages piped from a program.
Similar to a logfile, but it's mostly in-memory, and meant to be current.

This base program doesn't do much, it needs a library to consume data streaming from it via pipes. Paired with [cl-echoer.konsole-bash](https://gitlab.com/akashadutchie/cl-echoer.konsole-bash) you can for watch for program state changes on a side monitor

## Usage
 1. call make-echoer and store it, this will generate the pipes and identifiers
 2. call message to pipe strings into those pipes
 3. call update to get data back from a program consuming those pipes

