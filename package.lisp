(uiop:define-package #:cl-echoer
  (:use #:cl #:alexandria)
  (:export #:echoer
           #:make-echoer
           #:message 
           #:highlight 
           #:update
           
           #:file-differentiator-string
           #:dir
           #:auto-track
           #:tracked
           #:host->num   
           #:hostnum->process
           
           #:track-filepath   
           #:writeback-filepath 
           #:autotrack
           
           #:%highlight  
           #:%try-initialise
           #:track-copy-filepath))
